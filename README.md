# Escalado Horizontal

Ejemplo de balanceador de carga de red 

1. El servicio de AWS Load Balancing debemos de dirigirnos dentro del AWS Console a Instancias y allí encontraremos la opción de Load Balancer en la columna de la izquierda, cómo podeis ver en la siguiente imagen.

2. Colocar un nombre identificativo para el balanceo, en que red (VPC) queremos crearlo, el puerto 80 es el que va a realizar el balanceo permitiendo las conexiones HTTP.

3. Especifique las zonas de disponibilidad que se habilitarán para el balanceador de carga. El balanceador de carga direcciona el tráfico a los destinos de estas zonas de disponibilidad únicamente. Puede especificar solo una subred por zona de disponibilidad. También puede agregar una dirección IP elástica por zona de disponibilidad si desea tener direcciones específicas para el balanceador de carga.

https://drive.google.com/file/d/1mE1Ds2ZRDCjBnqv9RXjWEpcGpJb5d4lr/view?usp=sharing

https://drive.google.com/file/d/1urxvSsZTusO-JrzTh0K-0UQ79Tvuww5e/view?usp=sharing

4. Configuración del enrutamiento
Cancelar Anterior Siguiente: Registrar destinos
Su balanceador de carga enruta las solicitudes a los destinos de este grupo de destino mediante el protocolo y el puerto que especifique, y realiza verificaciones de estado en los destinos mediante esta configuración de verificación de estado. El grupo de destino que especifique en este paso se aplicará a todos los oyentes configurados en este balanceador de carga; puede editar los escuchas y agregar escuchas después de que se crea el balanceador de carga.

https://drive.google.com/file/d/1LXpJPBkm0fExf84fwBNkp-SLlu_CvYGl/view?usp=sharing

5. Registre destinos en el grupo de destino. Si registra un destino en una zona disponibilidad, el balanceador de carga comienza a direccionar las solicitudes a los destinos tan pronto como se completa el proceso de registro y el destino supera las comprobaciones de estado iniciales. (Ip de las instancias creadas)
 
https://drive.google.com/file/d/1U7crExJmCTM9sJ8-MKtOz4dCDfUWGuHv/view?usp=sharing

6. Revise los detalles del balanceador de carga antes de continuar

https://drive.google.com/file/d/1ksAghm2C3BlIe7DFhlEDvDwIdCezx8ns/view?usp=sharing

7. Finalmente obtenemos el balanceador de carga de red 

https://drive.google.com/file/d/10viPcmP54hQ0r0V_m8zt13_tIUgLNPEm/view?usp=sharing
